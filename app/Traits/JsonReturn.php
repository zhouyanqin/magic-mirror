<?php
namespace App\Traits;

use App\Contracts\Message;

trait JsonReturn {

    /**
     * @param array $data
     * @param null $msg
     * @param null $d_msg
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonSuccess(array $data, $msg = null, $d_msg = null) {
        return $this->jsonReturn($data, 0, $msg, $d_msg);
    }

    /**
     * @param $code
     * @param $msg
     * @param null $d_msg
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonFailure($code, $msg, $d_msg = null) {
        return $this->jsonReturn([], $code, $msg, $d_msg);
    }

    /**
     * @param array $data
     * @param $code
     * @param $msg
     * @param $d_msg
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonReturn(array $data, $code, $msg, $d_msg) {
        return response()->json([
            'code' => $code,
            'msg' => $msg,
            'd_msg' => $d_msg,
            'data' => $data
        ])->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param Message $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonReturnMessage(Message $message) {
        return response()->json($message->toArray())->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}