<?php
namespace App\Traits;

trait InternalCounter {
    /**
     * @var int
     */
    protected $_counter;

    public function getCounter() :int {
        return $this->_counter;
    }

    public function setCounter(int $value): int {
        return $this->_counter = $value;
    }

    public function resetCounter() : int{
        return $this->_counter = 0;
    }

    public function incrCounter() {
        return ++$this->_counter;
    }

    /**
     * 计数器计数到一定数字则自动触发
     * @param callable $function
     */
    public function triggerCounter(callable $function) {
        if ($this->incrCounter() >= $this->getCounterTriggerValue()) {
            call_user_func($function);
            $this->resetCounter();
        }
    }

    abstract public function getCounterTriggerValue();
}