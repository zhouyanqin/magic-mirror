<?php
/**
 * Created by PhpStorm.
 * User: kangjianing
 * Date: 2020/6/14
 * Time: 下午1:54
 */

namespace App\Models;

class EnglishWord extends Model {
    protected $table = 'english_words';
    protected $guarded = ['id'];


    public function wordList() {
        return $this->belongsTo(EnglishWordList::class, 'list_id');
    }
}