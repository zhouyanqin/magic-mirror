<?php
namespace App\Models;

/**
 * Class FutureWeather
 * @package App\Models
 * @property WeatherPosition $position
 */
class FutureWeather extends Model {
    protected $table = 'future_weathers';
    protected $guarded = ['id'];

    public function position(){
        return $this->belongsTo(WeatherPosition::class, 'position_id');
    }
}