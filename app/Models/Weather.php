<?php
namespace App\Models;

/**
 * Class Weather
 * @package App\Models
 * @property WeatherPosition $position
 */
class Weather extends Model {
    protected $table = 'weathers';
    protected $guarded = ['id'];

    public function position() {
        return $this->belongsTo(WeatherPosition::class, 'position_id');
    }

}