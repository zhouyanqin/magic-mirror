<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class WeatherPosition
 * @package App\Models
 * @property Weather $weather
 * @property Collection $futureWeathers
 * @property float $longitude
 * @property float $latitude
 */
class WeatherPosition extends Model {

    const KM_LONGITUDE_RANGE = 5.0;//定义范围，左右各差多少公里
    const KM_LATITUDE_RANGE = 5.0;
    protected $table = 'weather_positions';


    public function weather()
    {
        return $this->hasOne(Weather::class, 'position_id');
    }

    public function futureWeathers() {
        return $this->hasMany(FutureWeather::class, 'position_id');
    }

    /**
     * @param string $date Y-m-d format
     * @return FutureWeather|null
     */
    public function getFutureWeatherByDate($date) {
        return $this->futureWeathers()->firstWhere('date', $date);
    }
//max和min两个字段没有用了
//    public function setLatitudeAttribute(float $latitude) {
//        $range = self::getLongitudeRange();
//        $this->attributes['max_latitude'] = $latitude + $range;//大约5.5km
//        $this->attributes['min_latitude'] = $latitude - $range;
//        $this->attributes['latitude'] = $latitude;
//    }
//
//    public function setLongitudeAttribute(float $longitude) {
//        $range = self::getLongitudeRange();
//        $this->attributes['max_longitude'] = $longitude + $range;
//        $this->attributes['min_longitude'] = $longitude - $range;
//        $this->attributes['longitude'] = $longitude;
//    }

// 相关功能挪到WeatherPositionService里去了
//    public function save(array $options = [])
//    {
//        //first try to find the nearby position
////        $position = $this->where('min_longitude', '<=', $this->attributes['longitude'])->where('max_longitude', '>=', $this->attributes['longitude'])
////            ->where('min_latitude', '<=', $this->attributes['latitude'])->where('max_latitude', '>=', $this->attributes['latitude'])->first();
//        $position = self::findByGeoLocation($this->attributes['longitude'], $this->attributes['latitude']);
//        if ($position) {
//            //说明有则直接更新
//            $this->exists = true;
//            $this->id = $position->id;
//            return parent::save($options);
//        }
//        return parent::save($options);
//    }

    public static function getLongitudeRange(float $longitude = null, float $latitude = null) {
        //TODO 当有经纬度的时候计算实际的数值
        return (float) self::KM_LONGITUDE_RANGE / 111;
    }

    public static function getLatitudeRange(float $longitude = null, float $latitude = null) {
        //TODO 当有经纬度的时候计算实际的数值
        return (float) self::KM_LATITUDE_RANGE / 111;
    }
}