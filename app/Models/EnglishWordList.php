<?php
/**
 * Created by PhpStorm.
 * User: kangjianing
 * Date: 2020/6/14
 * Time: 下午2:08
 */
namespace App\Models;

class EnglishWordList extends Model {
    protected $table = 'english_word_lists';
    protected $guarded = ['id'];


    public function words() {
        return $this->hasMany(EnglishWord::class, 'list_id');
    }
}