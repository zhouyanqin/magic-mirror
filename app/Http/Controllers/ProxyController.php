<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProxyController extends Controller
{
    //
    public function getUrl(Request $request) {
        $url = $request->get('url');
        $response = Http::timeout(5)->get($url);
        if ($response->ok()) {
            return $response->body();
        }
        return $response->clientError();
    }
}
