<?php
namespace App\Http\Controllers;

class IndexController extends Controller
{
    public function index()
    {

        //天气组件所需要
        return view('home', ['appConfig' => $this->getAppConfig()]);
    }

    /**
     * 在android app中使用
     */
    public function app() {
        return view('mobile', ['appConfig' => $this->getAppConfig()]);
    }

    protected function getAppConfig() {
        $appConfig = [];
        //全局
        $appConfig['socket_server_host'] = $_SERVER['SERVER_ADDR'] . ':' . env('SOCKET_SERVER_PORT');
        $appConfig['geo_location'] = env('DEFAULT_LOCATION');
        $appConfig['mqtt_server_host'] = env('MQTT_SERVER_HOST', $_SERVER['SERVER_ADDR']) . ':' . env('MQTT_SERVER_WS_PORT', 9001);
        return $appConfig;
    }
}