<?php
namespace App\Http\Controllers\API;

use App\Models\Weather;
use App\Models\WeatherPosition;
use App\Services\WeatherPositionService;
use App\Services\WeatherService;
use Illuminate\Http\Request;

class WeatherController extends BaseController {
    /**
     * @var WeatherService
     */
    protected $weatherService;
    /**
     * @var WeatherPositionService
     */
    protected $weatherPositionService;
    public function index(Request $request, WeatherPositionService $weatherPositionService, WeatherService $weatherService) {
        $this->weatherService = $weatherService;
        $this->weatherPositionService = $weatherPositionService;
        $location = $request->get('position', env('DEFAULT_LOCATION'));
        $position = $weatherPositionService->findByGeoLocation(...explode(',', $location));
        switch ($request->get('type', null)) {
            case 'forecast':
                return $this->getForecast($position);
                break;
            case 'current':
                return $this->getCurrent($position);
                break;
            default:
                return $this->getWeatherInfo($position);
                break;
        }
    }

    protected function getWeatherInfo(WeatherPosition $position) {
//        $weather = $this->weatherService->getWeatherByPosition($position);
        $weather = $position->weather;
        $futureWeathers = $position->futureWeathers;
        $data = ['current' => $weather->attributesToArray()];
        foreach ($futureWeathers as $futureWeather) {
            $data['future'][] = $futureWeather->attributesToArray();
        }
        return $this->jsonSuccess($data);
    }
    /**
     * 获得当天的天气预报
     */
    protected function getCurrent(WeatherPosition $position) {
        /**
         * @var $weather Weather
         */
        $weather = $this->weatherService->getWeatherByPosition($position);
        return $this->jsonSuccess($weather->attributesToArray());
    }

    /**
     * 获得
     */
    protected function getForecast(WeatherPosition $position) {
        $futureWeathers = $position->futureWeathers;
        $data = [];
        foreach ($futureWeathers as $futureWeather) {
            $data[] = $futureWeather->attributesToArray();
        }
        return $this->jsonSuccess($data);
    }
}