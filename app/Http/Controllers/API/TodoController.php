<?php
namespace App\Http\Controllers\API;

use App\Services\TodoService;
use Illuminate\Http\Request;

class TodoController extends BaseController {

    public function index(Request $request, TodoService $todoService) {
        $list = $todoService->getTodoList();
        return $this->jsonSuccess($list);
    }
}