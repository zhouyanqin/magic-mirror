<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\JsonReturn;

abstract class BaseController extends Controller {
    use JsonReturn;
}