<?php
namespace App\Contracts;

/**
 * Interface Message
 * @package App\Contracts
 * @property-read int $code
 * @property-read string|null msg
 * @property-read string|null d_msg
 * @property-read array|null $data
 */
interface Message extends \ArrayAccess {
    const AvailableKeys = ['code', 'msg', 'd_msg', 'data'];

    /**
     * 只能先使用App::make创建再调用init
     * 因为App::make初始化类的时候，简单类型没办法传递进去（对象能不能我没试）
     * @param int $code
     * @param string|null $msg
     * @param string|null $d_msg
     * @param null $data
     * @return mixed
     */
    public function init(int $code, string $msg = null, string $d_msg = null, $data = null);

    /**
     * 获取data里面的某一个值
     * @param $key
     * @return mixed
     */
    public function getData($key);
    /**
     * @return array
     */
    public function toArray();
}