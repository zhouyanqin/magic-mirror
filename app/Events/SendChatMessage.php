<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use PHPSocketIO\SocketIO;
use Workerman\Mqtt\Client;
use Workerman\Timer;

class SendChatMessage
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public static $start_of_time;//@TODO 自动修改这个时间
    const START_OF_TIME = 1590000000;
    /**
     * Create a new event instance.
     *
     * @param SocketIO $server
     * @param \App\Services\ServerStatus $serverStatus
     */
    public function __construct(SocketIO $server)
    {
        $server->on('workerStart', function () use ($server) {
            $serverStatus = new \App\Services\ServerStatus();
            //            var_dump(env('MQTT_SERVER_HOST', '127.0.0.1') . ':' . env('MQTT_SERVER_PORT', 1883));
            $this->mqttConnect();
            Timer::add(0.5, function () use ($server, $serverStatus) {
//                echo 'fired!';
                /****************** Server Status ******************************/
                $serverStatus->triggerCounter(function () use ($server, $serverStatus) {
                    $server->emit('server status', json_encode($serverStatus->getServerStatus()));
                });
                /****************** Check commands ******************************/
                $commandFile = storage_path('command');
                if (!file_exists($commandFile) || filesize($commandFile) == 0) {
//                    echo 'no command';
                } else {
                    $content = trim(file_get_contents($commandFile));
                    $retryTimes = 0;
                    if (strpos($content, '||')) {
                        list($retryTimes, $command) = explode('||', $content, 2);
                    } else {
                        $command = $content;
                    }

                    if ($retryTimes) {
                        $retryTimes --;
                    }

                    if ($retryTimes) {
                        file_put_contents($commandFile, sprintf('%d||%s', $retryTimes, $command));
                    } else {
                        file_put_contents($commandFile, '');
                    }
                    if ($command) {
                        echo "发送命令$command\n";
                        $server->emit('current', $command);
                    }
                }
//                try {
//                    $fp = fopen($commandFile, 'r+b');
//                    $retry = 0;
//                    while (!($lock = flock($fp, LOCK_EX)) && $retry < 3) {
//                        usleep(1000);//等待0.01秒然后再试
//                        echo 'retry';
//                        $retry ++;
//                    }
//                    if (!$lock) {
//                        throw new \Exception('can not get file lock');
//                    }
//                    $content = fread($fp, filesize($commandFile));
//                    $retryTimes = 0;
//                    if (strpos($content, '||')) {
//                        list($retryTimes, $command) = explode('||', $content, 2);
//                    } else {
//                        $command = $content;
//                    }
//                    //写回去
//                    if ($retryTimes) {
//                        $retryTimes --;
//                    }
//                    if ($retryTimes) {
//                        $content = sprintf('%d||%s', $retryTimes, $command);
//                    } else {
//                        $content = '';
//                    }
////                    fseek($fp, 0);
//                    rewind($fp);
//                    if (!fwrite($fp, $content)) {
//                        throw new \Exception('can not write into command file');
//                    };
//                    flock($fp, LOCK_UN);//释放锁
//                    fclose($fp);
//                    unset($fp);
//                    //一切都没问题了，发送消息
//                    if ($command) {
//
//                        $server->emit('current', $command);
//                    }
//                } catch (\Exception $exception) {
//                    echo $exception->getMessage() . "\n";
//                } finally {
//                    if (isset($fp)) {
//                        flock($fp, LOCK_UN);//释放锁
//                        fclose($fp);
//                    }
//                }
                /****************** countdowns ******************************/
                $countdowns = config('countdown');
                $messages = [];
                $now = microtime(true);
                foreach ($countdowns as $code => $countdown) {
                    $topic = $countdown['topic']['name'];
                    $key = str_replace('/', '_', $topic);
                    $countdown = Cache::get($key);
                    if ($countdown) {
                        $countdown = json_decode($countdown, true);
                        if ($now - $countdown['end_time'] > 1800) {
                            Cache::delete($key);//删除
                            break;
                        }
                        $countdown['now'] = $now;
                        $countdown['s_now'] = $countdown['now'] - self::START_OF_TIME;
                        if ($countdown['status'] == 'running') {
                            $countdown['time_left'] = $countdown['end_time'] - $countdown['now'];
                        }
                        $countdown['s_start_time'] = $countdown['start_time'] - self::START_OF_TIME;
                        $countdown['s_end_time'] = $countdown['end_time'] - self::START_OF_TIME;
                        $messages[$code] = $countdown;
                    }
                }
                if (count($messages)) {
                    $server->emit('countdowns', json_encode($messages));
                }
            });
        });
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    protected function mqttConnect() {
//        var_dump(__LINE__);
        $countdowns = config('countdown');
        $mqtt = new Client('mqtt://' . env('MQTT_SERVER_HOST', '127.0.0.1') . ':' . env('MQTT_SERVER_PORT', 1883));
        $mqtt->onConnect = function($mqtt) use ($countdowns) {
            foreach ($countdowns as $code => $countdown) {
                $mqtt->subscribe($countdown['topic']['name']);
            }
        };
        $mqtt->onMessage = function($topic, $content) use ($countdowns) {
            $storeCountdown = function($topic, $content) {
                $key = str_replace('/', '_', $topic);
                if (in_array(substr($content, 0, 1), ['+', '-'])) {
                    $countdown = Cache::get($key);
                    $time = (int)substr($content, 1);
                    if ($countdown) {
                        $countdown = json_decode($countdown, true);
                        switch (substr($content, 0, 1)) {
                            case '+':
                                $countdown['end_time'] += $time;
                                break;
                            case '-':
                                $countdown['end_time'] -= $time;
                                if ($countdown['end_time'] < $countdown['start_time']) {
                                    $countdown['end_time'] = $countdown['start_time'];
                                }
                                break;
                        }
                    } else {
                        $now = microtime(true);
                        $countdown = ['start_time' => $now, 'end_time' => $now + (int) $time, 'status' => 'running'];
                    }
                    Cache::set($key, json_encode($countdown));
                } elseif (ctype_digit((string) $content)) {
                    //说明要开始
                    $now = microtime(true);
                    $countdown = ['start_time' => $now, 'end_time' => $now + (int) $content, 'status' => 'running'];
                    Cache::set($key, json_encode($countdown));
                } elseif ($content == 'stop') {
                    //停止
                    $countdown = Cache::get($key);
                    if ($countdown) {
                        $countdown = json_decode($countdown, true);
                        $countdown['status'] = 'stopped';
                        Cache::set($key, json_encode($countdown));
                    }
                }
            };
            foreach ($countdowns as $code => $countdown) {
                if ($topic == $countdown['topic']['name']) {
                    $storeCountdown($topic, $content);
                    break;
                }
            }
        };
        $mqtt->connect();
    }
}