<?php
/**
 * @deprecated 没用了
 */
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use PHPSocketIO\SocketIO;
use Workerman\Timer;

class ServerStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SocketIO $server, \App\Services\ServerStatus $serverStatus)
    {
        $server->on('workerStart', function() use($server, $serverStatus) {
            //可能只能同时有一个计数器，所以这个没有用
            Timer::add(5, function () use($server, $serverStatus) {
                $server->emit('server status', json_encode($serverStatus->getServerStatus()));
            });
        });
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
