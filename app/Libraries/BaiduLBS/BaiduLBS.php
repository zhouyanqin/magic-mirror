<?php
namespace BaiduLBS;

use Illuminate\Support\Facades\Http;

class BaiduLBS {

    protected $endPoint;
    protected $ak;
    protected $sk;
    protected $version;

    public function __construct()
    {
        $this->endPoint = config('baidu.lbs.end_point');
        $this->ak = config('baidu.lbs.ak');
        $this->sk = config('baidu.lbs.sk');
        $this->version = config('baidu.lbs.version');
    }

    public function reverseGeoCoding(float $longitude, float $latitude, string $coordType = 'wgs84ll') {
        $method = 'reverse_geocoding';
        $httpMethod = 'GET';
        $params = [
            'coordtype' => $coordType,
            'location' => sprintf('%f,%f', $latitude, $longitude),
            'extensions_poi' => 0,
        ];
        return $this->performRequest($method, $params, $httpMethod);
    }
    protected function performRequest(string $method, array $params = [], string $httpMethod = 'GET') {
        $params = $params + ['output' => 'json', 'ak' => $this->ak];
        $sn = $this->caculateAKSN($this->ak, $this->sk, sprintf('/%s/v%d/', $method, $this->version), $params, $httpMethod);
        $url = sprintf('%s/%s/v%d/', $this->endPoint, $method, $this->version);
        if ($httpMethod == 'GET') {
            $params['sn'] = $sn;
            $url .= '?' . http_build_query($params);
            $response = Http::timeout(5)->get($url);
        } elseif ($httpMethod == 'POST') {
            $response = Http::timeout(5)->post($url, $params);
        } else {
            return null;
        }
        if ($response->ok()) {
            $data = $response->json();
            return $data;
        }
        return null;
    }

    protected function caculateAKSN($ak, $sk, $url, $querystring_arrays, $method = 'GET')
    {
        if ($method === 'POST'){
            ksort($querystring_arrays);
        }
        $querystring = http_build_query($querystring_arrays);
        return md5(urlencode($url.'?'.$querystring.$sk));
    }
}