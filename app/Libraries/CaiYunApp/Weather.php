<?php
namespace CaiYunApp;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Weather {
    /**
     * @param float $longitude
     * @param float $latitude
     * @return array | false 成功返回数组，失败返回false
     */
    public function getWeatherByLocation(float $longitude, float $latitude) {
        $endPoint = Config::get('weather.end_point');
        $token = Config::get('weather.token');

        $url = sprintf('%s/%s/%f,%f/weather.json?alert=true', $endPoint, $token, $longitude, $latitude);
//        Log::info($url);
        $response = Http::timeout(5)->get($url);
        if (!$response->ok()) {
            Log::notice('请求错误，Code:' . $response->status());
            return false;
        }
        $data = $response->json();
        if ($data['status'] != 'ok') {
            log::notice('天气API返回错误！');
            return false;
        }
        return $data;
    }

}