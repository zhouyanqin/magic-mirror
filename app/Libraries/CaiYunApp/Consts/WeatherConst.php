<?php
namespace CaiYunApp\Consts;

final class WeatherConst {
    private static $skycons = [
        'CLEAR_DAY' => '晴',
        'CLEAR_NIGHT' => '晴',
        'PARTLY_CLOUDY_DAY' => '多云',
        'PARTLY_CLOUDY_NIGHT' => '多云',
        'CLOUDY' => '阴',
        'LIGHT_HAZE' => '轻度雾霾',
        'MODERATE_HAZE' => '中度雾霾',
        'HEAVY_HAZE' => '重度雾霾',
        'LIGHT_RAIN' => '小雨',
        'MODERATE_RAIN' => '中雨',
        'HEAVY_RAIN' => '大雨',
        'STORM_RAIN' => '暴雨',
        'FOG' => '雾',
        'LIGHT_SNOW' => '小雪',
        'MODERATE_SNOW' => '中雪',
        'HEAVY_SNOW' => '大雪',
        'STORM_SNOW' => '暴雪',
        'DUST' => '浮尘',
        'SAND' => '沙尘',
        'WIND' => '大风',
        'THUNDER_SHOWER' => '雷阵雨',
        'HAIL' => '冰雹',
        'SLEET' => '雨夹雪',
        'HAZE' => '霾',
    ];

    private static $ultraviolet_life_index = [
        -1 => '未知',
        0 => '无（夜间）',
        1 => '很弱',
        2 => '很弱',
        3 => '弱',
        4 => '弱',
        5 => '中等',
        6 => '中等',
        7 => '强',
        8 => '强',
        9 => '很强',
        10 => '很强',
        11 => '很强',
    ];

    private static $comfort_life_index = [
        -1 => '未知',
        0 => '闷热',
        1 => '酷热',
        2 => '很热',
        3 => '热',
        4 => '温暖',
        5 => '舒适',
        6 => '凉爽',
        7 => '冷',
        8 => '很冷',
        9 => '寒冷',
        10 => '极冷',
        11 => '刺骨的冷',
        12 => '湿冷',
        13 => '干冷',
    ];

    private static $alert_name = [
        '01' => '台风',
        '02' => '暴雨',
        '03' => '暴雪',
        '04' => '寒潮',
        '05' => '大风',
        '06' => '沙尘暴',
        '07' => '高温',
        '08' => '干旱',
        '09' => '雷电',
        '10' => '冰雹',
        '11' => '霜冻',
        '12' => '大雾',
        '13' => '霾',
        '14' => '道路结冰',
        '15' => '森林火灾',
        '16' => '雷雨大风',
    ];

    private static $alert_color = [
        '01' => '蓝色',
        '02' => '黄色',
        '03' => '橙色',
        '04' => '红色',
    ];

    public static function getSkyConName(string $code) : string {
        return isset(self::$skycons[$code]) ? self::$skycons[$code] : '未知';
    }

    /**
     * @param int $index
     * @return string
     * @deprecated 没用了
     */
    public static function getUltravioletLifeIndexName(int $index) : string {
        return isset(self::$ultraviolet_life_index[$index]) ? self::$ultraviolet_life_index[$index] : '未知';
    }

    /**
     * @param int $index
     * @return string
     * @deprecated 没用了
     */
    public static function getComfortLifeIndexName(int $index) : string {
        return isset(self::$comfort_life_index[$index]) ? self::$$comfort_life_index[$index] : '未知';
    }

    public static function getWindDirectionName(float $degree) : string {
        if ($degree >= 337.5) {
            return '北';
        } elseif ($degree >= 292.5) {
            return '西北';
        } elseif ($degree >= 247.5) {
            return '西';
        } elseif ($degree >= 202.5) {
            return '西南';
        } elseif ($degree >= 157.5) {
            return '南';
        } elseif ($degree >= 112.5) {
            return '东南';
        } elseif ($degree >= 67.5) {
            return '东';
        } elseif ($degree >= 22.5) {
            return '东北';
        } else {
            return '北';
        }
    }

    public static function getAlertCodeName($code) {
        return self::$alert_name[substr($code, 0 , 2)] . self::$alert_color[substr($code, 2)];
    }
}