<?php
namespace CaiYunApp;

class Config {

    public static function get(string $key) : string {
        return config('caiyun.' . $key);
    }
}