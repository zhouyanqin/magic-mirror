<?php

namespace AMap;

use Illuminate\Support\Facades\Http;

class AMapLBS
{

    private $key;
    private $endPoint;
    private $version;

    public function __construct()
    {
        $this->key = config('amap.key');
        $this->endPoint = config('amap.end_point');
        $this->version = config('amap.version');
    }

    public function getReverseGeo(float $longitude, float $latitude, array $params = [])
    {
        if (!isset($params['output'])) {
            $params['output'] = 'json';
        }
        if ($params['output'] != 'xml') {
            $params['output'] = 'json';
        }
        $params['key'] = $this->key;
        $params['location'] = sprintf('%f,%f', $longitude, $latitude);
        $url = sprintf('%s/v%d/geocode/regeo?%s', $this->endPoint, $this->version, http_build_query($params));
        $response = Http::timeout(5)->get($url);
        if ($response->ok()) {
            if ($params['output'] == 'xml') {
                $data = $response->body();
                return null;
                //TODO 暂不支持
            } else {
                return $response->json();
            }
        }
    }

    public function getTrafficStatus(float $longitudeLeftBottom, float $latitudeLeftBottom, float $longitudeRightTop, float $latitudeRightTop, array $params = [])
    {
        if (!isset($params['output'])) {
            $params['output'] = 'json';
        }
        if ($params['output'] != 'xml') {
            $params['output'] = 'json';
        }
        $params['key'] = $this->key;
        $rectangle = sprintf('%f,%f;%f,%f', $longitudeLeftBottom, $latitudeLeftBottom, $longitudeRightTop, $latitudeRightTop);
        $url = sprintf('%s/v%d/traffic/status/rectangle?%s&rectangle=%s', $this->endPoint, $this->version, http_build_query($params), $rectangle);
        var_dump($url);
        $response = Http::timeout(5)->get($url);
        if ($response->ok()) {
            if ($params['output'] == 'xml') {
                $data = $response->body();
                return null;
                //TODO 暂不支持
            } else {
                return $response->json();
            }
        }
    }
}