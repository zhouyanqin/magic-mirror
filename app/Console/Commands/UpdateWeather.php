<?php

namespace App\Console\Commands;

use App\Models\WeatherPosition;
use App\Services\WeatherService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class UpdateWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '后台自动更新天气信息';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param WeatherService $weatherService
     * @return mixed
     */
    public function handle(WeatherService $weatherService)
    {
        /**
         * @var $positions Collection
         * @var $position WeatherPosition
         */
        $positions = WeatherPosition::where('updated_at', '>', Carbon::now()->subDays(3)->toDateTimeString())->get();
        $this->info("Find positions\n");
        $bar = $this->output->createProgressBar(count($positions));
        $bar->start();
        foreach ($positions as $position) {
            $this->info("Updating location:" . $position->longitude . ' ' . $position->latitude);
            $this->info($weatherService->updateWeather($position) ? " OK!\n" : "Failed!\n");
            $bar->advance();
        }
        $bar->finish();
    }
}
