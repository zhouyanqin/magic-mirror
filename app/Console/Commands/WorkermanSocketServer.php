<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPSocketIO\SocketIO;
use Workerman\Timer;
use Workerman\Worker;

class WorkermanSocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workerman:socket-server {action} {--daemonize}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        global $argv;
        $action = $this->argument('action');
        $argv[0] = $this->signature;
        $argv[1] = $action;
        if ($this->option('daemonize')) {
            $argv[2] = '-d';
        }
        switch ($action) {
            case 'start':
                $port = config('workerman.server.port');
                $events = config('workerman.events');
                $server = new SocketIO($port);
                foreach ($events as $event) {
                    new $event($server);
                }
                Worker::runAll();
                break;
            case 'stop':
                $argv[1] = 'stop';
                Worker::runAll();
                break;
            case 'status':
                $this->info(Worker::getStatus());
                break;
            default:
                $this->error($action . ' action does not exist, try one one thoses : start, stop, status');
                break;
        }
    }
}
