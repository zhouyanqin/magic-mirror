<?php
namespace App\Services\RTB;

interface Contract {
    /**
     * 获取所有线路
     * @param array $options 可能的参数
     * @return array
     */
    public function getBusRoutes(array $options = []) : array;

    /**
     * 根据线路查询方向
     * @param string $route 线路标识
     * @return array
     */
    public function getRouteDirections(string $route) : array;

    /**
     * 获得所有的站点
     * @param string $route
     * @param string $direction 线路方向，从getRouteDirections中获取
     * @return array
     */
    public function getRouteStations(string $route, string $direction) : array;

    /**
     * 获得实时
     * @param string $route
     * @param string $direction
     * @param string $station
     * @return mixed
     */
    public function getRealTimeBus(string $route, string $direction, string $station);
}