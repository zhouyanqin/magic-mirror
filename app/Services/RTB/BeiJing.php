<?php
namespace App\Services\RTB;
use Illuminate\Support\Facades\Http;
use PHPHtmlParser\Dom;

class BeiJing implements Contract {
    //get real time bus info from bjbus.com
    private $endPoint = 'http://www.bjbus.com/home/ajax_rtbus_data.php';
    public function getBusRoutes(array $options = []): array
    {
        // TODO: Implement getBusRoutes() method.
    }

    public function getRouteDirections(string $route): array
    {
        // TODO: Implement getRouteDirections() method.
        $url = $this->endPoint . '?act=getLineDirOption&selBLine=' . $route;
        $response = Http::timeout(5)->get($url);
        if ($response->ok()) {
            $body = $response->body();
            $matches = [];
            preg_match_all('/<option value=\"(\d+)\">([^<>]+)<\/option>/i', $body, $matches);
            $directions = [];
            foreach ($matches[1] as $index => $key) {
                $directions[$key] = $matches[2][$index];
            }
            return $directions;
        }
        return [];
    }

    public function getRouteStations(string $route, string $direction): array
    {
        $url = $this->endPoint . '?act=getDirStationOption&selBLine=' . $route . '&selBDir=' . $direction;
        $response = Http::timeout(5)->get($url);
        if ($response->ok()) {
            $body = $response->body();
            $matches = [];
            preg_match_all('/<option value=\"(\d+)\">([^<>]+)<\/option>/i', $body, $matches);
            $stops = [];
            foreach ($matches[1] as $index => $key) {
                $stops[$key] = $matches[2][$index];
            }
            return $stops;
        }
        return [];
    }

    /**
     * 获取实时公交信息
     * @param string $route 线路名称，比如684
     * @param string $direction 线路方向，从getRouteDirections里来
     * @param string $station_id 站点id，来自getRouteStations
     * @return array|mixed
     * @throws \PHPHtmlParser\Exceptions\ChildNotFoundException
     * @throws \PHPHtmlParser\Exceptions\CircularException
     * @throws \PHPHtmlParser\Exceptions\NotLoadedException
     * @throws \PHPHtmlParser\Exceptions\StrictException
     */
    public function getRealTimeBus(string $route, string $direction, string $station_id)
    {
        $url = $this->endPoint . '?act=busTime&selBLine=' . $route . '&selBDir=' . $direction . '&selBStop=' . $station_id;
        $response = Http::timeout(5)->get($url);
        $result = [];
        if ($response->ok()) {
            $result['status'] = 'ok';
            $data = $response->json();
            $html = $data['html'];
//            var_dump($html);exit();
            //接下来使用paquettg/php-html-parser来解析html
            $dom = new Dom();
            $dom->loadStr($html);
            /**
             * @var $node Dom\HtmlNode
             */
            $node = $dom->getElementById('cc_stop');
            $lists = $node->find('li');
            $stations = [];
            $busPositions = [];
            foreach ($lists as $list) {
                /**
                 * @var $list Dom\HtmlNode
                 */
                //拿到div
                $div = $list->firstChild();
                //根据id来获得具体是什么信息
                $id = $div->getAttribute('id');
                if (ctype_digit((string) $id)) {
                    //说明是站点节点, id是站点id
                    $station = ['id' => $id, 'name' => $div->find('span')[0]->innerHtml()];
                    if ($id == $station_id) {
                        $station['is_c'] = 1;
                    }
                    $stations[$id] = $station;
                    $buss = $div->find('.buss');
                    if (count($buss)) {
                        //说明有在站点的车
                        foreach ($buss as $bus) {
                            //不知道clstag干什么用的，放进来再说
                            $busPosition = ['type' => 'i', 'position' => $id, 'clstag' => $bus->getAttribute('clstag')];
                            $busPositions[] = $busPosition;
                        }
                    }
                } else {
                    //说明是途中
                    $id = (int)$id;//这个id为下一站id
                    $busc = $div->find('.busc');
                    if (count($busc)) {
                        foreach ($busc as $bus) {
                            //不知道clstag干什么用的，放进来再说
                            $busPosition = ['type' => 'o', 'position' => $id, 'clstag' => $bus->getAttribute('clstag')];
                            $busPositions[] = $busPosition;
                        }
                    }
                }
            }
            $result['stations'] = $stations;
            $result['positions'] = $busPositions;
            return $result;
        }
        $result['status'] = 'error';
        return $result;
    }

}