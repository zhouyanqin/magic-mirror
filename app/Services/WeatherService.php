<?php
namespace App\Services;
use App\Models\FutureWeather;
use App\Models\Weather;
use App\Models\WeatherPosition;
use CaiYunApp\Consts\WeatherConst;
use CaiYunApp\Weather as WeatherSDK;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

class WeatherService {

    /**
     * 根据地点信息从接口更新天气数据
     * @param WeatherPosition $position
     * @return Weather|bool
     */
    public function updateWeather(WeatherPosition $position) {
//        $url = "https://api.caiyunapp.com/v2.5/TAkhjf8d1nlSlspN/121.6544,25.1552/weather.json";
        $sdk = new WeatherSDK();
        $data = $sdk->getWeatherByLocation($position->longitude, $position->latitude);
        if (!$data) {
            Log::notice('更新天气失败');
            return false;
        }
        Log::info('天气数据更新于' . date('Y-m-d H:i:s', $data['server_time']));

        $weather = $this->getWeatherByPosition($position, false);
        if (!$weather) {
            $weather = new Weather();
            $weather->position_id = $position->id;
        }
        //更新数据
        //首先是实时天气
        $realtimeData = $data['result']['realtime'];
        $modelData = [];
        foreach (explode(',', 'temperature,humidity,apparent_temperature,pressure,visibility,skycon') as $key) {
            $modelData[$key] = $realtimeData[$key];
        }
        $modelData['timestamp'] = $data['server_time'];
        $modelData['wind_direction'] = $realtimeData['wind']['direction'];
        $modelData['wind_direction_name'] = WeatherConst::getWindDirectionName($modelData['wind_direction']);
        $modelData['wind_speed'] = $realtimeData['wind']['speed'];
        $modelData['skycon_name'] = WeatherConst::getSkyConName($modelData['skycon']);
        $modelData['ultraviolet_index'] = (int)$realtimeData['life_index']['ultraviolet']['index'];
        $modelData['ultraviolet_name'] = $realtimeData['life_index']['ultraviolet']['desc'];
        $modelData['comfort_index'] = (int)$realtimeData['life_index']['comfort']['index'];
        $modelData['comfort_name'] = $realtimeData['life_index']['comfort']['desc'];
        foreach (explode(',', 'pm25,pm10,o3,no2,so2,co') as $key) {
            $modelData['aqi_' . $key] = $realtimeData['air_quality'][$key];
        }
        $modelData['aqi_cn'] = $realtimeData['air_quality']['aqi']['chn'];
        $modelData['aqi_cn_name'] = $realtimeData['air_quality']['description']['chn'];
        $modelData['aqi_us'] = $realtimeData['air_quality']['aqi']['usa'];
        $modelData['aqi_us_name'] = $realtimeData['air_quality']['description']['usa'];
        //未来两小时
        $modelData['precipitation_by_mins'] = json_encode($data['result']['minutely']['precipitation_2h']);
        $modelData['probability_by_half_hour'] = json_encode($data['result']['minutely']['probability']);
        $modelData['minutely_description'] = $data['result']['minutely']['description'];
        //未来24小时逐小时预报
        $hourlyData = $data['result']['hourly'];
        $modelData['hourly_description'] = $hourlyData['description'];
        $modelDataByHours = [];
        $hourlyKeys = ['precipitation_by_hours', 'temperature_by_hours', 'wind_speed_by_hours', 'wind_direction_by_hours',
            'wind_direction_name_by_hours', 'humidity_by_hours', 'skycon_by_hours', 'skycon_name_by_hours',
            'pressure_by_hours', 'visibility_by_hours', 'aqi_by_hours', 'aqi_pm25_by_hours'];
        foreach (['precipitation', 'temperature', 'wind', 'humidity', 'skycon', 'pressure', 'visibility'] as $key) {
            foreach ($hourlyData[$key] as $value) {
                $datetime = Date::createFromTimeString($value['datetime'])->toDateTimeString();
                if (!isset($modelDataByHours[$datetime])) {
                    $modelDataByHours[$datetime] = [];
                }
                switch ($key) {
                    case 'wind':
                        $modelDataByHours[$datetime]['wind_speed_by_hours'] = $value['speed'];
                        $modelDataByHours[$datetime]['wind_direction_by_hours'] = $value['direction'];
                        $modelDataByHours[$datetime]['wind_direction_name_by_hours'] = WeatherConst::getWindDirectionName($value['direction']);
                        break;
                    default:
                        $modelDataByHours[$datetime][$key . '_by_hours'] = $value['value'];
                        if ($key == 'skycon') {
                            $modelDataByHours[$datetime]['skycon_name_by_hours'] = WeatherConst::getSkyConName($value['value']);
                        }
                        break;
                }
            }
        }
        foreach ($hourlyData['air_quality']['aqi'] as $value) {
            $datetime = Date::createFromTimeString($value['datetime'])->toDateTimeString();
            if (!isset($modelDataByHours[$datetime])) {
                $modelDataByHours[$datetime] = [];
            }
            $modelDataByHours[$datetime]['aqi_by_hours'] = $value['value']['chn'];
        }
        foreach ($hourlyData['air_quality']['pm25'] as $value) {
            $datetime = Date::createFromTimeString($value['datetime'])->toDateTimeString();
            if (!isset($modelDataByHours[$datetime])) {
                $modelDataByHours[$datetime] = [];
            }
            $modelDataByHours[$datetime]['aqi_pm25_by_hours'] = $value['value'];
        }
        ksort($modelDataByHours);
        //开始整理数据
        if (!function_exists('array_key_first')) {//7.3才开始有的函数
            $keys = array_keys($modelDataByHours);
            $hourlyEndTime = end($keys);
            $modelData['hourly_start_time'] = reset($keys);
        } else {
            $modelData['hourly_start_time'] = array_key_first($modelDataByHours);
            $hourlyEndTime = array_key_last($modelDataByHours);
        }
        //生成每隔一小时的时间，为了防止数据有空挡造成后面的数据不对
        $hourlyRange = Date::createFromTimeString($modelData['hourly_start_time'])->toPeriod($hourlyEndTime, '1 hour');
        $hourlyValues = [];
        foreach ($hourlyRange as $carbonDateTime) {
            $datetime = $carbonDateTime->toDateTimeString();
            foreach ($hourlyKeys as $hourlyKey) {
                if (!isset($modelDataByHours[$datetime][$hourlyKey])) {
                    $hourlyValues[$hourlyKey][] = null;
                } else {
                    $hourlyValues[$hourlyKey][] = $modelDataByHours[$datetime][$hourlyKey];
                }
            }
        }

        foreach ($hourlyKeys as $hourlyKey) {
            $modelData[$hourlyKey] = json_encode($hourlyValues[$hourlyKey], JSON_UNESCAPED_UNICODE);
        }

        //预警信息
        $modelData['alerts'] = json_encode([]);
        if (isset($data['result']['alert'])) {
            if ($data['result']['alert']['status'] == 'ok') {
                $alertContent = $data['result']['alert']['content'];
                $alerts = [];
                foreach ($alertContent as $content) {
                    $alerts[] = [
                        'code' => $content['code'],
                        'desc' => $content['description'],
                        'pubtime' => (int)$content['pubtimestamp'],
                        'code_name' => WeatherConst::getAlertCodeName($content['code']),
                        'title' => $content['title'],
                        'status' => $content['status'],
                        'source' => $content['source'],
                    ];
                }
                $modelData['alerts'] = json_encode($alerts, JSON_UNESCAPED_UNICODE);
            }
        }
        $weather->fill($modelData);
//        $weather->updated_at = Date::createFromTimestamp($data['server_time']);
        if (!$weather->save()) {
            Log::warning('更新天气时写入数据库错误！');
            return false;
        }
        Log::info('实时天气更新完成！');

        //未来天气
        $dailyData = $data['result']['daily'];
        $modelDataByDates = [];
        foreach (explode(',', 'temperature,humidity,visibility,precipitation') as $key) {
            foreach ($dailyData[$key] as $value) {
                $date = substr($value['date'], 0, 10);
                if (!isset($modelDataByDates[$date])) {
                    $modelDataByDates[$date] = [];
                }
                foreach (['max', 'min', 'avg'] as $k) {
                    $modelDataByDates[$date][$key . '_' . $k] = $value[$k];
                }
            }
        }
        foreach ($dailyData['wind'] as $value) {
            $date = substr($value['date'], 0, 10);
            if (!isset($modelDataByDates[$date])) {
                $modelDataByDates[$date] = [];
            }
            foreach (['max', 'min', 'avg'] as $k) {
                $modelDataByDates[$date]['wind_speed_' . $k] = $value[$k]['speed'];
                $modelDataByDates[$date]['wind_direction_' . $k] = $value[$k]['direction'];
                $modelDataByDates[$date]['wind_direction_name_' . $k] = WeatherConst::getWindDirectionName($value[$k]['direction']);
            }
        }
        foreach (['skycon', 'skycon_08h_20h', 'skycon_20h_32h'] as $key) {
            foreach ($dailyData[$key] as $value) {
                $date = substr($value['date'], 0, 10);
                if (!isset($modelDataByDates[$date])) {
                    $modelDataByDates[$date] = [];
                }
                switch ($key) {
                    case 'skycon':
                        $indexInDB = 'skycon';
                        $indexNameInDB = 'skycon_name';
                        break;
                    case 'skycon_08h_20h':
                        $indexInDB = 'skycon_day';
                        $indexNameInDB = 'skycon_day_name';
                        break;
                    case 'skycon_20h_32h':
                        $indexInDB = 'skycon_night';
                        $indexNameInDB = 'skycon_night_name';
                        break;
                }
                $modelDataByDates[$date][$indexInDB] = $value['value'];
                $modelDataByDates[$date][$indexNameInDB] = WeatherConst::getSkyConName($value['value']);
            }
        }
        foreach (['ultraviolet', 'carWashing', 'comfort'] as $key) {
            foreach ($dailyData['life_index'][$key] as $value) {
                $date = substr($value['date'], 0, 10);
                if (!isset($modelDataByDates[$date])) {
                    $modelDataByDates[$date] = [];
                }
                $modelDataByDates[$date][strtolower($key) . '_index']= (int)$value['index'];
                $modelDataByDates[$date][strtolower($key) . '_name']= $value['desc'];
            }
        }
        foreach ($dailyData['air_quality']['aqi'] as $value) {
            $date = substr($value['date'], 0, 10);
            if (!isset($modelDataByDates[$date])) {
                $modelDataByDates[$date] = [];
            }
            foreach (['max', 'min', 'avg'] as $k) {
                $modelDataByDates[$date]['aqi_' . $k] = $value[$k]['chn'];//只取中国的
            }
        }
        foreach ($dailyData['air_quality']['pm25'] as $value) {
            $date = substr($value['date'], 0, 10);
            if (!isset($modelDataByDates[$date])) {
                $modelDataByDates[$date] = [];
            }
            foreach (['max', 'min', 'avg'] as $k) {
                $modelDataByDates[$date]['aqi_pm25_' . $k] = $value[$k];
            }
        }
        foreach ($dailyData['astro'] as $value) {
            $date = substr($value['date'], 0, 10);
            if (!isset($modelDataByDates[$date])) {
                $modelDataByDates[$date] = [];
            }
            $modelDataByDates[$date]['sunrise'] = $value['sunrise']['time'];
            $modelDataByDates[$date]['sunset'] = $value['sunset']['time'];
        }
        $dates = array_keys($modelDataByDates);
        //首先删除
        FutureWeather::where('position_id', $position->id)->whereNotIn('date', $dates)->delete();
        //查询出已经有的
        $exists = FutureWeather::where('position_id', $position->id)->get();
        /**
         * @var $futureWeather FutureWeather
         */
        foreach ($exists as $futureWeather) {
            $futureWeather->fill($modelDataByDates[$futureWeather->date]);
//            $futureWeather->updated_at = Date::createFromTimestamp($data['server_time']);
            $futureWeather->save();
            unset($modelDataByDates[$futureWeather->date]);
        }
        foreach ($modelDataByDates as $date => $value) {
            $value['position_id'] = $position->id;
            $value['date'] = $date;
//            $value['updated_at'] = Date::createFromTimestamp($data['server_time']);
            $futureWeather = FutureWeather::create($value);
        }
        Log::info('未来天气更新完成！');

        return $weather;
    }

    /**
     * @param WeatherPosition $position
     * @param bool $autoUpdate //数据库中不存在的话就自动更新
     * @return Weather|null
     */
    public function getWeatherByPosition(WeatherPosition $position, bool $autoUpdate = false) {
        $weather = Weather::firstWhere('position_id', $position->id);
        if (!$autoUpdate || $weather) {
            return $weather;
        }
        if ($weather = $this->updateWeather($position)) {
            return $weather;
        }
        return null;
    }

}