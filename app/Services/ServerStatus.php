<?php

namespace App\Services;

use App\Traits\InternalCounter;
use Illuminate\Support\Facades\Log;

class ServerStatus {
    use InternalCounter;

    public function getCounterTriggerValue()
    {
        return 10;
    }

    public function getServerStatus() {
        $result = [];
        $content = '';
        $fp = popen('top -b -n 2', 'r');
        while(!feof($fp)){
            $content .= fread($fp, 1024);
        }
        pclose($fp);
        $lines = explode("\n", $content, 6);
        $matches = [];
        if (!preg_match('/top - ([0-9:]+) up (.*),\s+\d+ users,\s+load average:\s+(\d+\.\d+),\s+(\d+\.\d+),\s+(\d+\.\d+)/i', $lines[0], $matches)) {
            Log::warning("server status parse error:\n\t$line[0]");
            return [];
        }
        $result['uptime'] = $matches[2];
        $result['server_time'] = $matches[1];
        $result['load'] = [$matches[3], $matches[4], $matches[5]];
        $matches = [];
        if (preg_match('/Tasks:\s+(\d+)\s+total,\s+(\d+)\s+running,\s+(\d+)\s+sleeping,\s+(\d+)\s+stopped,\s+(\d+)\s+zombie/', $lines[1], $matches)) {
            $result['tasks']['total'] = $matches[1];
            $result['tasks']['running'] = $matches[2];
            $result['tasks']['sleeping'] = $matches[3];
            $result['tasks']['stopped'] = $matches[4];
            $result['tasks']['zombie'] = $matches[5];
        }
        //%Cpu(s):  1.5 us,  9.1 sy,  0.0 ni, 89.4 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
        if (preg_match('/\%Cpu\(s\):\s+(\d+\.\d+)\s+us,\s+(\d+\.\d+)\s+sy,\s+(\d+\.\d+)\s+ni,\s+(\d+\.\d+)\s+id,/', $lines[2], $matches)) {
            $result['cpu']['user'] = $matches[1];
            $result['cpu']['system'] = $matches[2];
            $result['cpu']['nice'] = $matches[3];
            $result['cpu']['idle'] = $matches[4];
        }
        //CPU温度
        $result['cpu']['temperature'] = @trim(file_get_contents('/sys/class/thermal/thermal_zone0/temp')) / 1000;
        //其他先不搞了，没地方显示

        //MiB Mem :   1965.2 total,    517.4 free,    587.5 used,    860.3 buff/cache
        //MiB Swap:    100.0 total,    100.0 free,      0.0 used.   1366.4 avail Mem
        return $result;
    }
}