<?php
namespace App\Services;

use FabianBeiner\Todoist\TodoistClient;

class TodoService {

    public function getTodoList() {
        $client = $this->getClient();
        return $client->getAllTasks();
    }

    /**
     * @return TodoistClient
     * @throws \FabianBeiner\Todoist\TodoistException
     */
    protected function getClient() {
        static $client;
        if ($client == null) {
            $client = new TodoistClient(config('todo.token'));
        }
        return $client;
    }
}