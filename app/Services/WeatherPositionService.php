<?php
namespace App\Services;

use AMap\AMapLBS;
use App\Models\WeatherPosition;
use BaiduLBS\BaiduLBS;
use Illuminate\Support\Facades\Date;

class WeatherPositionService {
    public function whereByGeoLocation(float $longitude, float $latitude) {
        $longitudeRange = $this->getLongitudeRange($longitude, $latitude);
        $latitudeRange = $this->getLatitudeRange($longitude, $latitude);
        return WeatherPosition::whereBetween('longitude', [$longitude - $longitudeRange, $longitude + $longitudeRange])
            ->whereBetween('latitude', [$latitude - $latitudeRange, $latitude + $latitudeRange]);
    }

    /**
     * @param float $longitude
     * @param float $latitude
     * @param bool $autoUpdate 是否自动更新
     * @return WeatherPosition
     */
    public function findByGeoLocation(float $longitude, float $latitude, bool $autoUpdate = true) {
        $position = $this->whereByGeoLocation($longitude, $latitude)->first();
        if (!$autoUpdate) {
            return $position;
        }
        if ($position) {
            //说明找到了，更新坐标已经updated_at时间
            $position->longitude = $longitude;
            $position->latitude = $latitude;
            $position->updated_at = Date::now();
            if (!$position->city) {
                $this->updatePositionAddress($position);
            }
            $position->save();
        }
        return $position;
    }

    /**
     * 按照经纬度查询，如果找不到则创建新的
     * @param float $longitude
     * @param float $latitude
     * @return WeatherPosition
     */
    public function findOrInsert(float $longitude, float $latitude) {
        if ($position = $this->findByGeoLocation($longitude, $latitude, true)) {
            return $position;
        }
        $position = new WeatherPosition();
        $position->longitude = $longitude;
        $position->latitude = $latitude;
        $this->updatePositionAddress($position);
        $position->save();
        return $position;
    }

    public function getLongitudeRange(float $longitude = null, float $latitude = null) {
        return WeatherPosition::getLongitudeRange($longitude, $latitude);
    }

    public function getLatitudeRange(float $longitude = null, float $latitude = null) {
        return WeatherPosition::getLatitudeRange($longitude, $latitude);
    }

    protected function updatePositionAddress(WeatherPosition $position) {
        //try to get the city info from Baidu LBS
//        $baiduLBSClient = new BaiduLBS();
//        $data = $baiduLBSClient->reverseGeoCoding($position->longitude, $position->latitude);
//        if (is_array($data) && $data['status'] == 0) {
//            $position->city = $data['result']['addressComponent']['city'];
//            $position->province = $data['result']['addressComponent']['province'];
//            $position->country = $data['result']['addressComponent']['country'];
//            $position->district = $data['result']['addressComponent']['district'];
//        }
        $aMapClient = new AMapLBS();
        $data = $aMapClient->getReverseGeo($position->longitude, $position->latitude);
        if (is_array($data) && $data['status']) {
            $position->city = $data['regeocode']['addressComponent']['city'] ? $data['regeocode']['addressComponent']['city'] : $data['regeocode']['addressComponent']['province'];
            $position->province = $data['regeocode']['addressComponent']['province'];
            $position->country = $data['regeocode']['addressComponent']['country'];
            $position->district = $data['regeocode']['addressComponent']['district'];
        }
        return true;
    }
}