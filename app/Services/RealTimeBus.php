<?php
namespace App\Services;

class RealTimeBus {
    private $city;
    public function __construct()
    {
    }

    public function setCity(string $city) : string {
        return $this->city = $city;
    }

    public function getCity() : string {
        return $this->city;
    }


}