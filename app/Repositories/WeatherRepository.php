<?php
namespace App\Repositories;
use App\Models\Weather;
use Prettus\Repository\Eloquent\BaseRepository as Base;

/**
 * Class WeatherRepository
 * @package App\Repositories
 * @deprecated not in use
 */
class WeatherRepository extends Base {
    public function model()
    {
        return Weather::class;
    }

}