<?php
return [
    'weather' => [
        'token' => env('CAIYUN_WEATHER_TOKEN', null),
        'end_point' => 'https://api.caiyunapp.com/v2.5',
    ]
];