<?php
return [
    'primary' => [
        'name' => '主倒计时',
        'topic' => [
            "name" => 'ESP32_1/countdown',
            "qos" => 0,
        ],
    ],
    'second' => [
        'name' => '第二计时器',
        'topic' => [
            "name" => 'ESP32_2/countdown',
            "qos" => 0,
        ],
    ],
    'test' => [
        'name' => '测试',
        'topic' => [
            'name' => 'test/countdown',
            'qos' => 0,
        ],
    ],
];