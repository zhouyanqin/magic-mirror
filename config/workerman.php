<?php

return [

    /**
     * Listen port for SocketIO client.
     */
    'server' => [
		'port' => env('SOCKET_SERVER_PORT'),
	],
	
	/**
	 * Events dispatched when SocketIO server is running.
	 */
	'events' => [
		App\Events\SendChatMessage::class,
	],
];