<?php
return [
    'lbs' => [
        'ak' => env('BAIDU_LBS_AK', null),
        'sk' => env('BAIDU_LBS_SK', null),
        'end_point' => 'http://api.map.baidu.com',
        'version' => 3,
    ],
];