<?php
return [
    'end_point' => 'https://restapi.amap.com',
    'version' => 3,
    'key' => env('AMAP_KEY'),
];