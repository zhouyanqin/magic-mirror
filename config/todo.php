<?php
return [
    'token' => env('TODO_TOKEN'),
    'sync_token' => env('TODO_SYNC_TOKEN', '*'),
];