import threading
import time
from threading import Timer
import RPi.GPIO as GPIO

from lib.ld3320 import LD3320

TIME_EXTEND = 30  # 人走之后延迟的时间
DISTANCE_THRESHOLD = 100

PIN_RELAY = 23  # 接继电器引脚
PIN_SR501 = 17  # SR501的数据

PIN_LED = 18  # 指示灯

PIN_SR04_TRIG = 127  # sr04 trig
PIN_SR04_ECHO = 122  # sr04 echo

PIN_LD3320_RST = 5
PIN_LD3320_IRQ = 6  # LD3320的中断引脚

PIN_TTP229_SCL = 27  # TTP229 SCL针，注意和sr04冲突了，要改掉一个
PIN_TTP229_SDO = 22  # TTP229 SDO 注意和sr04冲突了，要改掉一个

LD3320_KEYWORDS = ['tian qi', 'tian qi yu bao', 'gong jiao', 'gong jiao che', 'di tu']


def init():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_LED, GPIO.OUT)
    GPIO.output(PIN_LED, GPIO.LOW)


class DetectiveHuman(threading.Thread):
    def __init__(self):
        super(DetectiveHuman, self).__init__()

        # 初始化SR501和继电器 #########
        GPIO.setup(PIN_SR501, GPIO.IN)
        GPIO.setup(PIN_RELAY, GPIO.OUT)
        GPIO.output(PIN_RELAY, GPIO.HIGH)

        # 初始化SR04
        GPIO.setup(PIN_SR04_TRIG, GPIO.OUT)
        GPIO.setup(PIN_SR04_ECHO, GPIO.IN)

    def turn_on_light(self):
        GPIO.output(PIN_RELAY, GPIO.LOW)

    def turn_off_light(self):
        GPIO.output(PIN_RELAY, GPIO.HIGH)

    # 重新开始关灯计时 ########
    # def reset_off_light(self, timer=None):
    #     if timer is not None:
    #         timer.cancel()
    #     timer = Timer(TIME_EXTEND, self.turn_off_light)
    #     timer.start()
    #     return timer

    # SR04检测距离
    def distance(self):
        # 发送高电平信号到 Trig 引脚
        GPIO.output(PIN_SR04_TRIG, True)
        # 持续 10 us
        time.sleep(0.00001)
        GPIO.output(PIN_SR04_TRIG, False)
        start_time = time.time()
        stop_time = time.time()
        # 记录发送超声波的时刻1
        while GPIO.input(PIN_SR04_ECHO) == 0:
            start_time = time.time()

        # 记录接收到返回超声波的时刻2
        while GPIO.input(PIN_SR04_ECHO) == 1:
            stop_time = time.time()

        # 计算超声波的往返时间 = 时刻2 - 时刻1
        time_elapsed = stop_time - start_time
        # 声波的速度为 343m/s， 转化为 34300cm/s。
        distance = (time_elapsed * 34300) / 2
        return distance

    def run(self):
        print("开始检测...\n")
        timer = None
        while True:
            if GPIO.input(PIN_SR501) == GPIO.HIGH:
                # 逻辑有问题，应该是先发现人，然后10秒内检测距离，如果10秒距离都比较大则忽略这次发现人，进行下一次
                # 循环检测距离，直到10秒内距离都比较大则延迟30秒关闭显示器
                print("发现人！...\n")
                # 检测距离
                dist = self.distance()
                print(dist)
                count = 10
                while dist <= DISTANCE_THRESHOLD or count >= 0:
                    print(dist)
                    print(count)
                    if dist <= DISTANCE_THRESHOLD:
                        self.turn_on_light()
                        # self.reset_off_light(timer)
                        if timer is not None:
                            timer.cancel()
                        timer = Timer(TIME_EXTEND, self.turn_off_light)
                        timer.start()
                        print("重置timer")
                    time.sleep(1)
                    dist = self.distance()
                    if dist < DISTANCE_THRESHOLD or GPIO.input(PIN_SR501) == GPIO.HIGH:
                        # 如果距离小或者仍然检测到有人，则重置计数器
                        count = 10
                    else:
                        count -= 1
                time.sleep(1)
            else:
                time.sleep(1)


class VoiceRecognize(threading.Thread):
    def __init__(self):
        super(VoiceRecognize, self).__init__()
        GPIO.setup(PIN_LD3320_IRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def run(self):
        ld3320 = LD3320(port=0, device=0)
        ld3320.reset(PIN_LD3320_RST)
        ld3320.initASR()
        timer = None
        for index in range(len(LD3320_KEYWORDS)):
            result = ld3320.addKeyword(index, LD3320_KEYWORDS[index])
            print("Add keyword [%d] '%s' %s" % (index, LD3320_KEYWORDS[index], 'ok' if result else 'fail'))
        while True:
            result = ld3320.runASR()
            print("Start ASR %s" % 'ok' if result else 'fail')
            time.sleep(0.1)  # for stability
            try:
                GPIO.wait_for_edge(PIN_LD3320_IRQ, GPIO.FALLING)
                (found, index) = ld3320.handleInterrupt()
                print("Interrupt received, find recognize result...")
                if found:
                    # 发现了，开始执行指令
                    # 点亮提示小灯
                    GPIO.output(PIN_LED, GPIO.HIGH)
                    if timer is not None:
                        timer.cancel()
                    timer = Timer(5, lambda: GPIO.output(PIN_LED, GPIO.LOW))
                    timer.start()
                    print("keyword is '%s', index=%s" % (LD3320_KEYWORDS[index], index))
                else:
                    print("Can not recognize.")
            except RuntimeError as ex:
                print(ex)


class KeyPad(threading.Thread):
    def __init__(self):
        super(KeyPad, self).__init__()
        GPIO.setup(PIN_TTP229_SCL, GPIO.OUT)
        GPIO.setup(PIN_TTP229_SDO, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.output(PIN_TTP229_SCL, GPIO.HIGH)  # Has to be HIGH
        self.touch = 0

    def interrupt(self) -> int:
        self.touch = 0
        time.sleep(0.000005)  # 5us

        for i in range(16):
            GPIO.output(PIN_TTP229_SCL, GPIO.LOW)

            readout = GPIO.input(PIN_TTP229_SDO)
            if not readout:  # when you press the pad, readout will be 0
                self.touch = i + 1
                print("{}".format(self.touch))

            time.sleep(0.00001)  # 10us
            GPIO.output(PIN_TTP229_SCL, GPIO.HIGH)
            time.sleep(0.00001)  # 10us
        return self.touch

    def run(self) -> None:
        edge_detect = None
        while True:
            time.sleep(0.05)  # for stability
            try:
                edge_detect = GPIO.wait_for_edge(PIN_TTP229_SDO, GPIO.FALLING)
                if edge_detect is not None:
                    touch = self.interrupt()
                    if touch > 0:
                        write_to_command_file("button#" + str(self.touch))
                    edge_detect = None
            except RuntimeError as ex:
                print(ex)
                pass



def write_to_command_file(content):
    print(content)


def main():
    init()
    # t1 = DetectiveHuman()
    t2 = VoiceRecognize()
    t3 = KeyPad()
    # t1.start()
    t2.start()
    t3.start()
    # t1.join()
    t2.join()
    t3.join()


if __name__ == '__main__':
    main()
    # GPIO.cleanup()
