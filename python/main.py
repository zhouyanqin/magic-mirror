import os
import threading
import time
from threading import Timer
import RPi.GPIO as GPIO

from lib.ld3320 import LD3320

TIME_EXTEND = 30  # 人走之后延迟的时间
DISTANCE_THRESHOLD = 300  # 测量距离

PIN_SR501 = 17  # SR501的数据

PIN_SR04_TRIG = 128  # sr04 trig
PIN_SR04_ECHO = 223  # sr04 echo

PIN_LED = 18  # 指示灯

PIN_LD3320_RST = 5  # LD3320 Reset Pin
PIN_LD3320_IRQ = 6  # LD3320的中断引脚

PIN_TTP229_SCL = 27  # TTP229 SCL
PIN_TTP229_SDO = 22  # TTP229 SDO

LD3320_KEYWORDS = ['zhu ye', 'shou ye', 'tian qi', 'tian qi yu bao', 'gong jiao', 'gong jiao che', 'di tu']

WITH_LED = True
WITH_SR04 = True
WITH_TTP229 = True
WITH_LD3320 = True

timer = None

def init():
    GPIO.setmode(GPIO.BCM)
    if WITH_LED:
        GPIO.setup(PIN_LED, GPIO.OUT)
        GPIO.output(PIN_LED, GPIO.LOW)


class DetectiveHuman(threading.Thread):
    def __init__(self):
        super(DetectiveHuman, self).__init__()

        # 初始化SR501 #########
        GPIO.setup(PIN_SR501, GPIO.IN)

        # 初始化SR04
        if WITH_SR04:
            GPIO.setup(PIN_SR04_TRIG, GPIO.OUT)
            GPIO.setup(PIN_SR04_ECHO, GPIO.IN)

    # SR04检测距离
    @staticmethod
    def distance():
        # 发送高电平信号到 Trig 引脚
        GPIO.output(PIN_SR04_TRIG, True)
        # 持续 10 us
        time.sleep(0.00001)
        GPIO.output(PIN_SR04_TRIG, False)
        start_time = time.time()
        stop_time = time.time()
        # 记录发送超声波的时刻1
        while GPIO.input(PIN_SR04_ECHO) == 0:
            start_time = time.time()

        # 记录接收到返回超声波的时刻2
        while GPIO.input(PIN_SR04_ECHO) == 1:
            stop_time = time.time()

        # 计算超声波的往返时间 = 时刻2 - 时刻1
        time_elapsed = stop_time - start_time
        # 声波的速度为 343m/s， 转化为 34300cm/s。
        distance = (time_elapsed * 34300) / 2
        return distance

    @staticmethod
    def turn_on_screen():
        pass

    @staticmethod
    def turn_off_screen(self):
        pass

    def run(self):
        print("开始检测...\n")
        screen_timer = None
        while True:
            if GPIO.input(PIN_SR501) == GPIO.HIGH:
                # 逻辑有问题，应该是先发现人，然后10秒内检测距离，如果10秒距离都比较大则忽略这次发现人，进行下一次
                # 循环检测距离，直到10秒内距离都比较大则延迟30秒关闭显示器
                print("发现人！...\n")
                # 检测距离
                dist = self.distance()
                # print(dist)
                count = 10
                while dist <= DISTANCE_THRESHOLD or count >= 0:
                    # print(dist)
                    # print(count)
                    if dist <= DISTANCE_THRESHOLD:
                        self.turn_on_screen()
                        # self.turn_off_screen(screen_timer)
                        if screen_timer is not None:
                            screen_timer.cancel()
                            screen_timer = None
                        screen_timer = Timer(TIME_EXTEND, self.turn_off_screen)
                        screen_timer.start()
                        print("重置timer")
                    time.sleep(1)
                    dist = self.distance()
                    if dist < DISTANCE_THRESHOLD or GPIO.input(PIN_SR501) == GPIO.HIGH:
                        # 如果距离小或者仍然检测到有人，则重置计数器
                        count = 10
                    else:
                        count -= 1
                time.sleep(1)
            else:
                time.sleep(1)


class EventThread(threading.Thread):
    def __init__(self):
        super(EventThread, self).__init__()
        if WITH_LD3320:
            GPIO.setup(PIN_LD3320_IRQ, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            ld3320 = LD3320(port=0, device=0)
            ld3320.reset(PIN_LD3320_RST)
            ld3320.initASR()
            for index in range(len(LD3320_KEYWORDS)):
                result = ld3320.addKeyword(index, LD3320_KEYWORDS[index])
                print("添加关键词[%d] '%s' %s" % (index, LD3320_KEYWORDS[index], '成功' if result else '失败'))
            self.ld3320 = ld3320
        if WITH_TTP229:
            GPIO.setup(PIN_TTP229_SCL, GPIO.OUT)
            GPIO.setup(PIN_TTP229_SDO, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.output(PIN_TTP229_SCL, GPIO.HIGH)  # Has to be HIGH
            self.touch = 0

    def ttp229_interrupt(self) -> int:
        touched = 0
        time.sleep(0.000005)  # 5us

        for i in range(16):
            GPIO.output(PIN_TTP229_SCL, GPIO.LOW)

            readout = GPIO.input(PIN_TTP229_SDO)
            if not readout:  # when you press the pad, readout will be 0
                touched = i + 1
                # if self.touch != touched:
                    # print("{}".format(touched))

            time.sleep(0.00001)  # 10us
            GPIO.output(PIN_TTP229_SCL, GPIO.HIGH)
            time.sleep(0.00001)  # 10us
        return touched

    def run(self) -> None:
        if WITH_LD3320:
            GPIO.add_event_detect(PIN_LD3320_IRQ, GPIO.FALLING)
            result = self.ld3320.runASR()
            print("Start ASR %s" % 'ok' if result else 'fail')

        if WITH_TTP229:
            GPIO.add_event_detect(PIN_TTP229_SDO, GPIO.FALLING)

        while True:
            time.sleep(0.01)
            if WITH_LD3320:
                if GPIO.event_detected(PIN_LD3320_IRQ):
                    (found, index) = self.ld3320.handleInterrupt()
                    print("Interrupt received, find recognize result...")
                    if found:
                        # 发现了，开始执行指令
                        print("keyword is '%s', index=%s" % (LD3320_KEYWORDS[index], index))
                        write_to_command_file('voice', index)
                    else:
                        print("Can not recognize.")
                    result = self.ld3320.runASR()
                    print("Start ASR %s" % 'ok' if result else 'fail')

            if WITH_TTP229:
                if GPIO.event_detected(PIN_TTP229_SDO):
                    touched = self.ttp229_interrupt()
                    if touched and self.touch != touched:
                        write_to_command_file("button", touched)
                        self.touch = touched
                    elif touched == 0:
                        self.touch = 0


def write_to_command_file(command_type, payload):
    global timer
    content = ''
    if command_type is 'button':
        print('button %d pressed' % payload)
        content = 'button:' + str(payload)
    elif command_type is 'voice':
        #['zhu ye', 'shou ye', 'tian qi', 'tian qi yu bao', 'gong jiao', 'gong jiao che', 'di tu']
        if payload == 0 or payload == 1:
            content = 'component:home'
        elif payload == 2 or payload == 3:
            content = 'component:weather'
        elif payload == 4 or payload == 5:
            content = 'component:bus'
        elif payload == 6:
            content = 'component:a-map'
        else:
            pass

    if content != '':
        # 点亮提示小灯
        if WITH_LED:
            GPIO.output(PIN_LED, GPIO.HIGH)
            if timer is not None:
                timer.cancel()
            timer = Timer(3, lambda: GPIO.output(PIN_LED, GPIO.LOW))  # 亮3秒
            timer.start()
        filename = os.path.realpath('../storage/command')
        fp = open(filename, 'w')
        fp.write(content)
        fp.close()


def main():
    init()
    # t1 = DetectiveHuman()
    t2 = EventThread()
    # t1.start()
    t2.start()
    # t1.join()
    t2.join()


if __name__ == '__main__':
    main()
