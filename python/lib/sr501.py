import RPi.GPIO as GPIO
import time


channel = 17  # 引脚号4
led_channel = 23

time.sleep(1)  # 时延一秒

GPIO.setmode(GPIO.BCM)  # 以BCM编码格式
GPIO.setup(channel, GPIO.IN)
GPIO.setup(led_channel, GPIO.OUT)
#GPIO.output(led_channel, GPIO.LOW)
GPIO.output(led_channel, GPIO.HIGH)


try:
    print("开始检测...\n")
    while True:
        if GPIO.input(channel) == GPIO.HIGH:
            print("发现人！...\n")
            GPIO.output(led_channel, GPIO.LOW)
        else:
            print("检测中...\n")
            GPIO.output(led_channel, GPIO.HIGH)
        time.sleep(0.5)
except KeyboardInterrupt:
    print("Quit!")

GPIO.cleanup()
