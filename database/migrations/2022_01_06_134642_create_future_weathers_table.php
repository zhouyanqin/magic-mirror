<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFutureWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('future_weathers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('position_id');
            $table->string('date');
            $table->float('temperature_max');
            $table->float('temperature_min');
            $table->float('temperature_avg');
            $table->float('humidity_max');
            $table->float('humidity_min');
            $table->float('humidity_avg');
            $table->float('wind_speed_max');
            $table->float('wind_speed_min');
            $table->float('wind_speed_avg');
            $table->float('wind_direction_max');
            $table->float('wind_direction_min');
            $table->float('wind_direction_avg');
            $table->string('wind_direction_name_max');
            $table->string('wind_direction_name_min');
            $table->string('wind_direction_name_avg');
            $table->float('precipitation_max');
            $table->float('precipitation_min');
            $table->float('precipitation_avg');
            $table->float('visibility_max');
            $table->float('visibility_min');
            $table->float('visibility_avg');
            $table->string('skycon');
            $table->string('skycon_name');
            $table->string('skycon_day');
            $table->string('skycon_day_name');
            $table->string('skycon_night');
            $table->string('skycon_night_name');
            $table->unsignedSmallInteger('ultraviolet_index');
            $table->string('ultraviolet_name');
            $table->unsignedSmallInteger('comfort_index');
            $table->string('comfort_name');
            $table->unsignedSmallInteger('carwashing_index');
            $table->string('carwashing_name');
            $table->float('aqi_pm25_max');
            $table->float('aqi_pm25_min');
            $table->float('aqi_pm25_avg');
            $table->float('aqi_max');
            $table->float('aqi_min');
            $table->float('aqi_avg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('future_weathers');
    }
}
