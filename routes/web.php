<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('test', 'TestController@test');
Route::get('proxy/get', 'ProxyController@getUrl');
Route::get('app', 'IndexController@app');
Route::get('english', 'EnglishController@index');