import VueSocketIO from 'vue-socket.io'
import Vuex from 'vuex'
import VueTouch from 'vue-touch'
import VueLocalStorage from 'vue-localstorage'
// import VueRouter from 'vue-router'
// import Vue from 'vue'
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./function.js');
require('./bootstrap');

window.Vue = require('vue');
window.mqtt = require('mqtt');
// window.echarts = require('echarts/lib/echarts');
// window.io = require('socket.io-client');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components/', true, /\.vue$/i)
// console.log(files.keys());
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('ExampleComponent', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(new VueSocketIO({
	debug: false,
	connection: server_config.socket_server_host,
}));

Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		config: server_config,
		weather_data: null,
		mqtt_data: null,
		sensors: [],
	},
	mutations: {
		weather(state, payload) {
			state.weather_data = payload;
		},
		mqtt(state, payload) {
			state.mqtt_data = payload;
		},
		config(state, payload) {
			state.config = payload;
		},
		sensor(state, payload) {
			state.sensors = payload;
		},
	}
});

Vue.use(VueLocalStorage);

// Vue.use(VueRouter);
// const router = new VueRouter({
// 	mode: 'hash',
// 	routes: [{
// 		path: '/example',
// 		// component: require('./components/ExampleComponent.vue').default,
// 		component: Vue.component('example-component'),
// 	},
// 	// {
// 	// 	path: '/weather',
// 	// 	component: WeatherComponent
// 	// },
// 	],
// });

Vue.use(VueTouch, {
	name: 'v-touch'
})

const appVue = new Vue({
	el: '#app',
	data: {
		// comName: 'weather-component',
		comName: null,
		command: '',
		server_status: [],
	},
	store,
	localStorage: {
		mqtt_countdown: {
			type: Object,
			default: {
				status: 'stopped',
				start_time: 0,
				end_time: 0,
			}
		}
	},
	// router,
	computed: {
		currentTabComponent: function() {
			return this.comName
		}
	},
	socket: {
		connect(data) {
			console.log('连接成功');
		},
		current(data) {
			console.log(data);
		}
	},
	mounted() {
		// this.sockets.emit('connect', 1);
		this.sockets.subscribe('current', (data) => {
			var index = data.indexOf(":");
			if (index == '-1') {
				console.log('Unknown command!', data);
				return;
			}
			var type = data.substr(0, index);
			var value = data.substr(index + 1);
			switch (type) {
				case 'component':
					if (this.isValidComponent(value)) {
						this.comName = value + '-component';
						this.command = ''; //清除上一次命令
						console.log("组件:" + this.comName);
					}
					break;
				case 'command':
					this.command = value;
					// if(value == 'force-update') {
					// 	this.$forceUpdate();
					// }
					console.log("命令" + this.command);
					break;
				default:
					console.log('unknown data: ' + data);
			}
		});
		this.sockets.subscribe('server status', function(data) {
			this.server_status = $.parseJSON(data);
		});
		// console.log('test:', this.$options.components['ExampleComponent']);
		// console.log(window.location.hash);
		{
			let hash = window.location.hash;
			hash = hash.substring(2);
			if (this.isValidComponent(hash)) {
				this.comName = hash + '-component';
			} else {
				this.comName = 'home-component';
			}
		}
		// console.log(hash);
		// 	switch (window.location.hash) {
		// 		case '#/bus':
		// 			this.comName = 'BusComponent';
		// 			break;
		// 		case '#/a-map':
		// 			this.comName = 'AMapComponent';
		// 			break;
		// 		default:
		// 			this.comName = 'WeatherComponent';
		// 	}
	},
	methods: {
		isValidComponent: function(name) {
			return (name + '-component').ucfirst().toCamelCase() in this.$options.components;
		},
		onSwipe: function(swipe) {
			let _this = this;
			console.log(swipe);
			//针对地图组件
			if (_this.comName == 'a-map-component') {
				//如果中心点在map-container上则忽略
				// let container = document.getElementById('map-container');
				let containerRect = this.$refs.currentComponent.$refs.mapContainer.getBoundingClientRect();
				if (containerRect.left < swipe.center.x && swipe.center.x < containerRect.right && containerRect.top < swipe.center
					.y && swipe.center.y < containerRect.bottom) {
					return;
				}

			}
			let components = ['home', 'weather', 'a-map', 'bus'];
			let index = 0;
			for (index in components) {
				if (components[index] == _this.comName.replace('-component', '')) {
					break;
				}
			}
			// var DIRECTION_NONE = 1;
			// var DIRECTION_LEFT = 2;
			// var DIRECTION_RIGHT = 4;
			// var DIRECTION_UP = 8;
			// var DIRECTION_DOWN = 16; 
			switch (swipe.direction) {
				case 2: //
					index--;
					break;
				case 4:
					index++;
					break;
			}
			if (index < 0) {
				index = components.length - 1;
			} else if (index >= components.length) {
				index = 0;
			}
			_this.comName = components[index] + '-component';
		},
	},
	watch: {
		comName: function(newVal, oldVal) {
			window.location.hash = '#/' + newVal.lcfirst().toKebabCase().replace('-component', '');
		}
	}
});

// Vue.directive('touch', {
// 	bind: function(el, binding, vnode) {
// 		console.log(binding);
// 	}
// })
// // Vue.prototype.$echarts = require('echarts/lib/echarts');
// Vue.prototype.$comName = appVue.$data.comName

// Date.prototype.format = function(format) {
// 	var args = {
// 		"M+": this.getMonth() + 1,
// 		"d+": this.getDate(),
// 		"h+": this.getHours(),
// 		"m+": this.getMinutes(),
// 		"s+": this.getSeconds(),
// 		"q+": Math.floor((this.getMonth() + 3) / 3), //quarter

// 		"S": this.getMilliseconds()
// 	};
// 	if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
// 	for (var i in args) {
// 		var n = args[i];

// 		if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n)
// 			.substr(("" + n).length));
// 	}
// 	return format;
// };
