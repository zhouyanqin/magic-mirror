
String.prototype.ucfirst = (function() {
	var cache = {};
	return function() {
		let str = this;
		var ret = cache[str];
		if (ret) {
			// console.log('cache called');
			return ret;
		} else {
			return cache[str] = str.substring(0, 1).toUpperCase() + str.substring(1);
		}
	}
})();

String.prototype.lcfirst = (function() {
	var cache = {};
	return function() {
		let str = this;
		var ret = cache[str];
		if (ret) {
			// console.log('cache called');
			return ret;
		} else {
			return cache[str] = str.substring(0, 1).toLowerCase() + str.substring(1);
		}
	}
})();

String.prototype.toKebabCase = (function() {
	var cache = {};
	return function() {
		let str = this;
		var ret = cache[str];
		if (ret) {
			// console.log('cache called');
			return ret;
		} else {
			return cache[str] = str.replace(/[A-Z]/g, function(i) {
				return '-' + i.toLowerCase();
			})
		}
	}
})();
String.prototype.toCamelCase = (function() {
	var cache = {};
	return function() {
		let str = this;
		var ret = cache[str];
		if (ret) {
			// console.log('cache called');
			return ret;
		} else {
			return cache[str] = str.replace( /-([a-z])/g, function( all, i ) {
                return i.toUpperCase();
			})
		}
	}
})();
