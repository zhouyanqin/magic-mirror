import VueSocketIO from 'vue-socket.io'
import Vuex from 'vuex'
import VueTouch from 'vue-touch'
import VueLocalStorage from 'vue-localstorage'
import VueRouter from 'vue-router'

require('./function.js');
require('./bootstrap');

window.Vue = require('vue');

const files = require.context('./mobiles/', true, /\.vue$/i)
// console.log(files.keys());
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.use(new VueSocketIO({
	debug: false,
	connection: server_config.socket_server_host,
}));

Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		config: server_config,
		weather_data: null,
		mqtt_data: null,
		sensors: [],
	},
	mutations: {
		weather(state, payload) {
			state.weather_data = payload;
		},
		mqtt(state, payload) {
			state.mqtt_data = payload;
		},
		config(state, payload) {
			state.config = payload;
		},
		sensor(state, payload) {
			state.sensors = payload;
		},
	}
});

Vue.use(VueLocalStorage);
Vue.use(VueTouch, {
	name: 'v-touch'
});

Vue.use(VueRouter);
const routes = [
	{
		path: '/',
		component: Vue.component('CountdownComponent'),
	},
	{
		path: '/home',
		component: Vue.component('HomeComponent'),
	},
	{
		path: '/countdown',
		component: Vue.component('CountdownComponent')
	},
];
const router = new VueRouter({
	linkActiveClass: 'active',
	routes
});

import Sensor from './config/sensors.js';
import Countdown from './config/countdown.js';
const appVue = new Vue({
	el: '#app',
	data: {
		server_status: [],
		component: null,
		hash: null,
	},
	store,
	router,
	localStorage: {
		// mqtt_countdown: {
		// 	type: Object,
		// 	default: {
		// 		status: 'stopped',
		// 		start_time: 0,
		// 		end_time: 0,
		// 	}
		// }
	},
	socket: {
		connect(data) {
			console.log('连接成功');
		},
		current(data) {
			console.log(data);
		}
	},
	mounted() {
		this.sockets.subscribe('server status', function(data) {
			this.server_status = $.parseJSON(data);
			// console.log(this.server_status);
		});
	},
	methods: {
		isValidComponent: function(name) {
			// console.log(this.$options.components);
			return (name + '-component').ucfirst().toCamelCase() in this.$options.components;
		},
		onSwipe: function(swipe) {},
		goBack() {
			window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/')
		},
	},
	computed: {
	},
	watch: {
	}
});
