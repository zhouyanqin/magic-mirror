@extends('layouts.app')

@section('title', 'Home')

@section('content')
	<header-component></header-component>
    <transition mode="out-in">
        <component v-bind:is="currentTabComponent" ref="currentComponent"></component>
    </transition>
	<div style="height: 3.125rem;"><!-- 占位专用 --></div>
	<footer-component></footer-component>
{{--	<button v-on:click="switchCom">切换</button>--}}
@endsection