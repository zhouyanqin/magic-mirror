<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">{{ config('app.name') }}</div>

                <div class="card-body">
                    <ul id="messages" class="list-group"></ul>

                    <form class="mt-5">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="message" autocomplete="off" placeholder="Message">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
    $(function(){
        var socket = io("http://192.168.1.13:{{ config('workerman.server.port') }}");
        socket.on('connect', function(){
            console.log('connect success');
        });
        // socket.on('connection', function(){
        //     console.log('connection success');
        // });
        // setTimeout(function () {
        //     console.log('init');
        //     socket.emit('test message', 'init');
        // }, 1000);
        socket.on('test message', function(message){
            console.log('get new message');
            $('#messages').append($('<li class="list-group-item">').text(message));
        });

        $('form').submit(function(){
            socket.emit('test message', $('#message').val());
            $('#message').val('');

            return false;
        });
    });
</script>
</body>
</html>