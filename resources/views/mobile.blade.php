@extends('layouts.mobile')

@section('title', 'Home')

@section('content')
	<!-- <component v-bind:is="currentTabComponent" ref="currentComponent"></component> -->
	<router-view></router-view>
    <footer-nav-component></footer-nav-component>
@endsection