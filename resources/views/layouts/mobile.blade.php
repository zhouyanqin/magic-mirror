<!-- 位于 resources/views/layouts/mobile.blade.php -->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Magic Mirror - @yield('title')</title>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/mobile.css') }}" />
    <script type="text/javascript">
        var server_config = @json($appConfig);
    </script>
</head>
<body>

<div id="app" class="d-flex flex-column h-100 container-fluid">
    <v-touch v-on:swipe="onSwipe" style="height: 100%">
        @yield('content')
    </v-touch>
</div>
<script type="text/javascript" src="{{ mix('js/mobile.js') }}"></script>
</body>
</html>